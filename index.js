//4-7
let trainer = {
	name: "Ash Ketchum",
	age: 3,
	pokemon: ['Pikachu','Bulbasaur','Charizard','Squirtle'],
	friends: {
		hoenn: ['May','Max'],
		kanto: ['Brock','Misty']
	},
	talk: function(pokemonNo){
		console.log(this.pokemon[pokemonNo]+"!, I choose you.");
		}
	}
console.log(trainer);

console.log("Result of dot notation:");
console.log(trainer.name);
console.log("Result of square bracket notation:");
console.log(trainer['pokemon']);
console.log("Result of talk method:");
trainer.talk(1);

//8	
function Pokemon(name,level){
	this.name = name;
	this.level = level;
	this.health = level * 2;
	this.attack =  level * 1;
	this.tackle = function(target){
		if(target.health - this.attack >0){
		console.log(`${this.name} tackled ${target.name}.`);
		console.log(`${target.name}'s health is now reduced to ${target.health - this.attack}`);
		}
		else{
			console.log(`${this.name} tackled ${target.name}.`);
			this.faint(target);
		}
	};
	this.faint = function(target){
		console.log(`${target.name} fainted.`);
	}
}

//9

let Pikachu = new Pokemon('Pikachu',12);
let Geodude = new Pokemon('Geodude',8);
let Mewtwo = new Pokemon('Mewtwo',100);

console.log(Pikachu,Geodude,Mewtwo);
